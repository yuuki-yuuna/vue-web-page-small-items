import InitUnit from '../pages/InitUnit.vue'
import BlogEdit from '../pages/BlogEdit.vue'
import DateLine from '../pages/DateLine.vue'
import PassageUnit from '../pages/PassageUnit.vue'
import AnimeUnit from '../pages/AnimeUnit.vue'
import AnimeDetail from '../pages/AnimeDetail.vue'
import GameUnit from '../pages/GameUnit.vue'
import MusicUnit from '../pages/MusicUnit.vue'
import DetailUnit from '../pages/DetailUnit.vue'
import InfoUnit from '../pages/InfoUnit.vue'
import AuthorUnit from '../pages/AuthorUnit.vue'

import VueRouter from 'vue-router'

export default new VueRouter ({
    routes: [
        {
            path: '/',
            component: InitUnit
        },
        {
            path: '/edit',
            component: BlogEdit
        },
        { 
            path: '/dateline',
            component: DateLine
        },
        {
            path: '/passage',
            component: PassageUnit
        },
        {
            path: '/anime',
            component: AnimeUnit
        },
        {
            path: '/AnimeDetail/:name',
            component: AnimeDetail
        },
        {
            path: '/game',
            component: GameUnit
        },
        {
            path: '/music',
            component: MusicUnit
        },
        {
            path: '/blog/:id',
            component: DetailUnit
        },
        {
            path: '/about',
            component: InfoUnit
        },
        {
            path: '/author',
            component: AuthorUnit
        }
    ]
})