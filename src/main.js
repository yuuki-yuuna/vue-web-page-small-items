import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import router from './script/router.js'
import {Autocomplete, Carousel, CarouselItem, Form, FormItem, Input} from 'element-ui'
import {RadioGroup, Radio, Button, Timeline, TimelineItem, Image} from 'element-ui'
import {Menu, MenuItem, Submenu, Dropdown, DropdownItem, DropdownMenu} from 'element-ui'
import {Pagination, Tabs, TabPane} from 'element-ui'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(mavonEditor)

Vue.use(Autocomplete)
Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(RadioGroup)
Vue.use(Radio)
Vue.use(Button)
Vue.use(Timeline)
Vue.use(TimelineItem)
Vue.use(Image)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)
Vue.use(Pagination)
Vue.use(Tabs)
Vue.use(TabPane)

new Vue({
  render: h => h(App),
  router: router
}).$mount('#app')

//匿名登录腾讯云数据库，顺便可以检查一下数据库的连接状态
import cloudbase from "@cloudbase/js-sdk"

const app = cloudbase.init({
  env: "web-test-6gd18uit30d6212a",
})

app.auth({
  persistence: 'session'
}).anonymousAuthProvider().signIn().then(() => {console.log('匿名登录成功!')}).catch(() => {console.log('匿名登录失败')})