# mywebsite

## 项目在线预览地址
```
https://web-test-6gd18uit30d6212a-1311610931.tcloudbaseapp.com/
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).